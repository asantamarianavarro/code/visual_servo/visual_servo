#ifndef _DEBUG_TOOLS_H
#define _DEBUG_TOOLS_H

#include <string>
#include <eigen3/Eigen/Dense>

class CDebug_Tools
{
  
  private:

    std::string datetime_; // Initial date and time

    /**
    * \brief Get date and time
    *
    * Get date and time as string
    *
    */    
    std::string get_datetime();

  public:

  	/**
    * \brief Constructor
    *
    * Debug Tools Class constructor.
    *
    */
    CDebug_Tools();

    /**
    * \brief Destructor
    *
    * Debug Tools Class destructor.
    *
    */
    ~CDebug_Tools();

    /**
    * \brief Write to file
    *
    * Write to file some vars to plot them externally. Input data must be an Eigen MatrixX
    * Stored data will be readable using Matlab/Octave "load" function.
    */
    void write_to_file(const std::string& folder_name, const std::string& file_name, const Eigen::MatrixXd& data, const double& ts);

};

#endif





