#include <debug_tools.h>

#include <stdio.h>
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <sstream>
#include <pwd.h>
#include <math.h>

#include <ctime>

CDebug_Tools::CDebug_Tools(void)
{
  this->datetime_ = get_datetime();
}

CDebug_Tools::~CDebug_Tools(void)
{
}

std::string CDebug_Tools::get_datetime()
{
  time_t rawtime;
  struct tm * timeinfo;
  char buffer[80];

  time (&rawtime);
  timeinfo = localtime(&rawtime);

  // strftime(buffer,80,"%d%m%Y_%I%M%S",timeinfo);
  strftime(buffer,80,"%d%m%Y_%I%M",timeinfo);
  std::string str(buffer);

  return str;
}

void CDebug_Tools::write_to_file(const std::string& folder_name, const std::string& file_name, const Eigen::MatrixXd& data, const double& ts)
{
  // Get home directory
  int myuid;
  passwd *mypasswd;
  std::string path;
  myuid = getuid();
  mypasswd = getpwuid(myuid);
  path= mypasswd->pw_dir;

  // Add Folder name
  std::stringstream folder;
  folder << folder_name;
  path+= folder.str();

  // Add Folder with date and time
  // path+="/";
  // path+=this->datetime_;

  // Check if path exists, and create folder if needed
  struct stat sb;
  if (stat(path.c_str(), &sb) != 0)
  {
    mkdir(path.c_str(), S_IRWXU|S_IRGRP|S_IXGRP);
    std::string command = "mkdir -p ";
    std::stringstream ss;
    ss << command << path;
    std::string com = ss.str();
    int ret = system(com.c_str());
    if (WEXITSTATUS(ret)!=0)
      std::cout << "Error creating directory." << std::endl;
  }

  path+="/";

  // Create file
  std::string FileName;
  FileName = path;
  FileName+= file_name;
  std::ofstream File;
  File.open(FileName.c_str(), std::ios::out | std::ios::app | std::ios::binary);

  // Write time stamp
  File << ts;
  File << ",";

  // Write data in Matlab format
  for (int jj = 0; jj < data.cols(); ++jj)
  {
    for (int ii = 0; ii < data.rows()-1; ++ii)
    {
    File << data(ii,jj);
    File << ",";    
    }
  }
  // last row
  for (int jj = 0; jj < data.cols()-1; ++jj)
  {
    File << data(data.rows()-1,jj);
    File << ",";
  }
  // last term
  File << data(data.rows()-1,data.cols()-1);
  File << "\n";
  File.close();
}
