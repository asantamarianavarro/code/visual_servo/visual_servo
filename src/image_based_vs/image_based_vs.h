#ifndef _IMAGE_BASED_VS_H
#define _IMAGE_BASED_VS_H
#include <stdio.h>
#include <string>
#include <iostream>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Eigenvalues>
#include <eigen3/Eigen/SVD>
#include <eigen3/Eigen/Core>

#include <pid.h>
#include <vector>

using namespace Eigen;
using namespace std;

struct img_attr {
  int width; // pixels
  int height;  // pixels
};


//!  Image-Based Visual Servoing Class 
/*!
  This Class Implements an Image-Based Visual servoing with Virtual features 
  projected to an input frame given by an Homogenous transform from the camera frame to the target
  to simplify object detection for example detecting a marker
*/

class CImage_Based_Vs
{
  
  private:

  	// Input vars
    bool traditional_; /*!< Select Traditional or Uncalibrated Jacobian */
    bool random_points_; /*!< Select random virtual features */
    MatrixXd V2_; /*!< Quadrotor pitch and roll velocities (to be extracted with the jacobian) */
    Matrix4d Tobj_in_cam_; /*!< Current object position in camera frame */
    Matrix4d Tobj_in_cam_x_; /*!< Desired final position */

    int n_; /*!< Number of virtual features */
  	int noise_; /*!< noise pixels */
  	bool planar_; /*!< Features Base planarity */
  	bool quadrotor_; /*!< Special quadrotor underactuated output (pitch and roll velocities to 0) */
  	double u0_,v0_; /*!< Image plane center distances */
  	double ff_; /*!< Online computed focal length */
  	MatrixXd base_in_cam_4d_; /*!< current features r.t current camera position */
  	MatrixXd base_in_cam_4d_x_; /*!< desired features r.t current camera position */

  	MatrixXd J_; /*!< Image Jacobian */
  	MatrixXd uv_; /*!< Image plane coordinates */
  	MatrixXd vel_; /*!< Camera 6dof velocities */

    img_attr img_attr_; // Image parameters

    //PID
    std::vector<PID> pid_; /*! PID controllers */

    /**
    * \brief PID
    *
    * PID update.
    *
    */
    // double updatePid(double& error,const double& dt,const double& i);

    /**
    * \brief Virtual Features
    *
    * Obtain the object features (virtual), its mean, its base points
    * and the linear factors respect to the base (alfas)
    *
    */
  	void virtual_features();

    /**
    * \brief Initialitzation with zeros
    *
    * Matrix structures initialization.
    */ 	
  	void init_zeros();

    /**
    * \brief Image projection
    *
    * Projection of 3D points (x,y,z) to image plane (u,v)
    */   
    MatrixXd image_projection(const float& fu, const float& fv, const MatrixXd& points); 
    /**
    * \brief Features vector
    *
    * Vector of features (x,y) in the camera frame to compute the error
    */    
	MatrixXd features_vector(const MatrixXd& points);

    /**
    * \brief Traditional Jacobian
    *
    * Traditional Jacobian that translates image feature velocities
    * to camera velocities
    */   	
	void jacobian_traditional();

	/**
    * \brief Uncalibrated Jacobian
    *
    * Uncalibrated Jacobian that translates image feature velocities
    * to camera velocities
    */  
	void jacobian_uncalibrated();

    /**
    * \brief 6DOF camera control (PID)
    *
    * Compute the 6DOF camer control (PID) of the visual servo
    *
    */ 
    void control();

    /**
    * \brief 4DOF quadrotor control (PID)
    *
    * Compute a special output for underactuated quadrotor with 
    * only 4 controllable DOF (Roll and Pitch velocities set to 0)
    *
    */ 
	void quadrotor_ctrl();

    /**
    * \brief Pi to Pi
    *
    * Set an angle between -pi and pi (rad).
    *
    */
    double pi2pi(double& angle);

    /**
    * \brief Camera frame change
    *
    * The computed velocities should be rotated to the camera frame,
    * with the camera looking north it is x = East, Y:Down, Z:North.
    */
    MatrixXd cam_frame_change(const MatrixXd& vel_base);

    /**
    * \brief Get homogenous transform matrix
    *
    * Get homogenous transform matrix from x,y,z,roll,pitch and yaw.
    *
    */
    Matrix4d get_HT(const double& x,const double& y,const double& z,const double& roll,const double& pitch,const double& yaw);

  public:

    // TODO: These variables are public for debug purposes. 
    // They need to be private with get and set functions.
    MatrixXd diff_; /*!< Difference between feature vectors */
    MatrixXd s_, s_x_; /*!< Current and desired features vectors */
    MatrixXd features_in_obj_,base_in_obj_3d_,base_in_obj_4d_,alfas_in_obj_; /*!< Features, mean, baricentric coordinates and alfas (for linear comb.) */
    MatrixXd feat_imgplane_, feat_x_imgplane_; /*! Features base projections in the image plane */
    
  	/**
    * \brief Constructor
    *
    * Image-Based Visual Servoing Class constructor.
    *
    */
    CImage_Based_Vs();

    /**
    * \brief Destructor
    *
    * Image-Based Visual Servoing Class destructor.
    *
    */
    ~CImage_Based_Vs();

    /**
    * \brief Image-Based Visual Servo Function
    *
    * Main public function call of Image-Based Visual Servo.
    *
    */    
    void image_based_vs(
        const img_attr& img_params,
        const bool& traditional, const bool& random_points,
        const MatrixXd& desired_pose, const MatrixXd& current_pose, 
        const double& dt, const MatrixXd& V2, 
        const bool& quadrotor, const MatrixXd& kp,
        const MatrixXd& kd, const MatrixXd& ki,
        const double& i_lim, MatrixXd& cam_vel);

    /**
    * \brief Get Virtual Features
    *
    * Public function to pull features (e.g. to print them externally)
    *
    */
    void get_features(MatrixXd& features, MatrixXd& base);
};

#endif
