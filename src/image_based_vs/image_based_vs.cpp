#include "image_based_vs.h"

using namespace Eigen;
using namespace std;


CImage_Based_Vs::CImage_Based_Vs()
{
  // Setup PIDs
  this->pid_.resize(6);
  for (int ii = 0; ii < this->pid_.size(); ++ii)
  {
    this->pid_[ii].windup_guard_ = 0.0;
    this->pid_[ii].kp_ = 0.0;
    this->pid_[ii].ki_ = 0.0;
    this->pid_[ii].kd_ = 0.0;
    this->pid_[ii].control_ = 0.0; 
    this->pid_[ii].reset();
  }

  virtual_features();
}
 
CImage_Based_Vs::~CImage_Based_Vs()
{
}

//! Main public API of Image-Based Visual Servo.
/*!
  \param traditional (bool): Jacobian type.
  \param random_points (bool): Virtual features generation.
  \param Tobj_in_cam_x (Eigen::MatrixXd [4,4]): Homogenous transform: Desired object position in camera frame.
  \param Tobj_in_cam_ (Eigen::MatrixXd [4,4]): Homogenous transform: Current object position in camera frame.
  \param dt (double): time interval for control.
  \param V2 (Eigen::MatrixXd [2,1]): Quadrotor pitch and roll angular velocities from sensor.
  \param quadrotor (bool): Activate special underactuated output for quadrotors.
  \param kp (Eigen::MatrixXd [6,1]): Proportional gain.
  \param kd (Eigen::MatrixXd [6,1]): Derivative gain.
  \param ki (Eigen::MatrixXd [6,1]): Integral gain.
  \param i_lim (double): Integration limits.
  \param cam_vel (Eigen::MatrixXd [6,1]): Output camera velocities.
  \return The camera velocities
*/
void CImage_Based_Vs::image_based_vs(
  const img_attr& img_params,
  const bool& traditional, const bool& random_points,
	const MatrixXd& desired_pose, const MatrixXd& current_pose, 
	const double& dt, const MatrixXd& V2, 
	const bool& quadrotor, const MatrixXd& kp,
	const MatrixXd& kd, const MatrixXd& ki,
	const double& i_lim, MatrixXd& cam_vel)
{
  this->img_attr_ = img_params;
  this->traditional_ = traditional;
  this->random_points_ = random_points;
  this->quadrotor_ = quadrotor;
  this->V2_ = V2;

  if (dt == 0.0)
  {
    for (int ii = 0; ii < this->pid_.size(); ++ii)
    {
      this->pid_[ii].reset();
      this->pid_[ii].kp_ = kp(ii,0);
      this->pid_[ii].ki_ = ki(ii,0);
      this->pid_[ii].kd_ = kd(ii,0);
      this->pid_[ii].windup_guard_ = i_lim;
    }

    init_zeros();
  }

  // Base coordinates in camera frame ___________________
  // Current features transform r.t.camera 
  this->Tobj_in_cam_ = get_HT(current_pose(0,0),current_pose(1,0),current_pose(2,0),current_pose(3,0),current_pose(4,0),current_pose(5,0));

  // Current features vector ____________________________
  // using marker detector pose to avoid velocities integration
  this->base_in_cam_4d_ = this->Tobj_in_cam_ * this->base_in_obj_4d_;

  this->s_ = features_vector(this->base_in_cam_4d_);

  // Desired features_vector ____________________________
  // Desiredz object pose in camera frame
  this->Tobj_in_cam_x_ = get_HT(desired_pose(0,0),desired_pose(1,0),desired_pose(2,0),desired_pose(3,0),desired_pose(4,0),desired_pose(5,0));
  this->base_in_cam_4d_x_ = this->Tobj_in_cam_x_ * this->base_in_obj_4d_; 

  // Desired features vector
  this->s_x_ = features_vector(this->base_in_cam_4d_x_);

  // Current features in image plane ____________________________
  // Features from camera frame 
  MatrixXd features_in_cam;
  features_in_cam = this->Tobj_in_cam_ * this->features_in_obj_;

  this->uv_ = image_projection(this->ff_,this->ff_,features_in_cam);

  // noise add
  MatrixXd ran(2,this->n_);
  ran.setRandom();
  this->uv_ = this->uv_ + this->noise_ * ran;
  
  // DEBUG: Project Base Features to image plane to plot them
  this->feat_imgplane_ = image_projection(this->ff_,this->ff_,this->base_in_cam_4d_);
  this->feat_x_imgplane_ = image_projection(this->ff_,this->ff_,this->base_in_cam_4d_x_);

  // Image Jacobian ____________________________________________
  if (this->traditional_){
    jacobian_traditional();
  }
  else {
    jacobian_uncalibrated();  
  }

  // Control Law _______________________________________________
  control(); 

  if (this->quadrotor_)
  {
  	quadrotor_ctrl();
  }

  if(!this->traditional_) cam_vel = cam_frame_change(this->vel_);
  else cam_vel = this->vel_;

  // PID control _______________________________________________
  for (unsigned int ii = 0; ii < 6; ++ii)
  {
    // //compute velocities with the PID
    // cam_vel(ii,0)=updatePid(cam_vel(ii,0), this->dt_, ii);  

    //compute control commands
    this->pid_[ii].update(-cam_vel(ii,0), dt);
    cam_vel(ii,0) = this->pid_[ii].control_;    
  }
}

//! Get Virtual features, mean points, barycentric coordinates and alfas.
void CImage_Based_Vs::virtual_features()
{
  // Number of features  
  this->n_ = 9;

	if (this->random_points_){  
    // Points of the object
    this->features_in_obj_ = MatrixXd::Zero(4,this->n_);
    this->features_in_obj_.block(0,0,3,this->n_) = MatrixXd::Random(3,this->n_);
    this->features_in_obj_.row(3) = VectorXd::Ones(this->n_);
  }
  else {
    this->features_in_obj_ = MatrixXd::Zero(4,this->n_);
    this->features_in_obj_.row(0) << -0.124725, -0.431413, 0.375723, 0.658402, -0.29928, 0.314608, -0.203127, -0.0350187, -0.70468;
    this->features_in_obj_.row(1) << 0.86367, 0.477069, -0.668052, -0.339326, 0.37334, 0.717353, 0.629534, -0.56835, 0.762124;
    this->features_in_obj_.row(2) << 0.86162, 0.279958, -0.119791, -0.542064, 0.912936, -0.12088, 0.368437, 0.900505, 0.282161;
    this->features_in_obj_.row(3) << 1, 1, 1, 1, 1, 1, 1, 1, 1;
  }

  this->base_in_obj_3d_ = MatrixXd::Zero(3,3); 
  this->base_in_obj_3d_(0,0) = this->features_in_obj_.row(0).mean();
  this->base_in_obj_3d_(1,0) = this->features_in_obj_.row(1).mean();
  this->base_in_obj_3d_(2,0) = this->features_in_obj_.row(2).mean();
  // Computing the base points
  MatrixXd q(3,this->n_);
  q = this->features_in_obj_.block(0,0,3,this->n_);
  q.colwise() -= this->base_in_obj_3d_.col(0);
  // Assemble the correlation matrix Q = q * q'
  Matrix3d Q = (q * q.transpose());
  // Compute the Singular Value Decomposition
  JacobiSVD<Matrix3d> svd (Q, ComputeFullU | ComputeFullV);
  //Matrix3d u = svd.matrixU ();
  Matrix3d v = svd.matrixV ();
  this->base_in_obj_3d_.col(1) = this->base_in_obj_3d_.col(0)+v.col(0);
  //with this library the second column of v appear sign changed
  this->base_in_obj_3d_.col(2) = this->base_in_obj_3d_.col(0)-v.col(1);

  this->base_in_obj_3d_ = this->base_in_obj_3d_/10;

  // check the planarity
  if ( svd.singularValues()(2) < 0.00001){
    this->planar_=true; 
  }
  else {
    this->planar_=false; 
  }

  MatrixXd dd1(3,1),dd2(3,1),dd3(3,1);       
  if (this->planar_){
    cout << "[Image-Based Visual Servo]: Planar Features" << endl;  
    this->base_in_obj_4d_ = MatrixXd::Zero(4,3);  
    this->base_in_obj_4d_.block(0,0,3,3) = base_in_obj_3d_;
    this->base_in_obj_4d_.row(3) << 1, 1, 1;  
 
    MatrixXd D(3,2),DD(2,2);
  
    dd1 = this->base_in_obj_3d_.col(1).array()-this->base_in_obj_3d_.col(0).array();
    dd2 = this->base_in_obj_3d_.col(2).array()-this->base_in_obj_3d_.col(0).array();
    D.col(0) = dd1;
    D.col(1) = dd2;
    DD = D.transpose()*D;
  
    // alfas linear combination of vectors
    this->alfas_in_obj_ = MatrixXd::Zero(3,this->n_);
    // alfas.resize(3,n);
    this->alfas_in_obj_.block(1,0,2,4) = (DD.inverse()*D.transpose()) * q;
    //alfas.block(1,0,2,4)=D.inverse() * q;
    this->alfas_in_obj_.row(0) = MatrixXd::Ones(1,this->n_)-this->alfas_in_obj_.block(1,0,2,this->n_).colwise().sum();
  }  
  else{
    cout << "[Image-Based Visual Servo]: NON Planar Features" << endl;
  
    // resizing the base points to 4
    Matrix3d Temp;
    Temp = this->base_in_obj_3d_;
    this->base_in_obj_3d_.resize(3,4);
    this->base_in_obj_3d_.block(0,0,3,3) = Temp;
    this->base_in_obj_3d_.col(3) = this->base_in_obj_3d_.col(0)+v.col(2);
    this->base_in_obj_4d_ = MatrixXd::Zero(4,4); 
    // P0.resize(4,4);
    this->base_in_obj_4d_.block(0,0,3,4) = this->base_in_obj_3d_;
    this->base_in_obj_4d_.row(3) << 1, 1, 1, 1;
    dd1 = this->base_in_obj_3d_.col(1).array()-this->base_in_obj_3d_.col(0).array();
    dd2 = this->base_in_obj_3d_.col(2).array()-this->base_in_obj_3d_.col(0).array();
    dd3 = this->base_in_obj_3d_.col(3).array()-this->base_in_obj_3d_.col(0).array();
    MatrixXd D(3,3),DD(2,2);
    D.col(0) = dd1;
    D.col(1) = dd2;
    D.col(2) = dd3;
    DD = D.transpose()*D;
    //alfas linear combination of vectors
    this->alfas_in_obj_ = MatrixXd::Zero(4,this->n_);
    // alfas.resize(4,n);
    this->alfas_in_obj_.block(1,0,3,this->n_) = (DD.inverse()*D.transpose()) * q;
    // alfas.block(1,0,3,4)=D.inverse() * q;
    this->alfas_in_obj_.row(0) = MatrixXd::Ones(1,this->n_)-this->alfas_in_obj_.block(1,0,3,this->n_).colwise().sum();  
  }
}

//! Initialize vars to zero
void CImage_Based_Vs::init_zeros()
{ 

  if (planar_){
    //actual features
    this->s_= MatrixXd::Zero(6,1);
    //desired features
    this->s_x_= MatrixXd::Zero(6,1);
    //Actual points camera frame
    this->base_in_cam_4d_= MatrixXd::Zero(4,3);
    //Desired points camera frame
    this->base_in_cam_4d_x_= MatrixXd::Zero(4,3);
    //Image Jacobian
    this->J_= MatrixXd::Zero(6,6);
    //Features vectors error
    this->diff_ = MatrixXd::Zero(6,1);
  }
  else {
    //actual features
    this->s_= MatrixXd::Zero(8,1);
    //desired features
    this->s_x_= MatrixXd::Zero(8,1);
    //Actual points camera frame
    this->base_in_cam_4d_= MatrixXd::Zero(4,4);
    //Desired points camera frame
    this->base_in_cam_4d_x_= MatrixXd::Zero(4,4);
    //Image Jacobian
    this->J_= MatrixXd::Zero(8,6);
    //Features vectors error
    this->diff_ = MatrixXd::Zero(8,1);
  }

  //image coordinates
  this->uv_ = MatrixXd::Zero(2,n_);  
  //Velocity commands
  this->vel_ = MatrixXd::Zero(6,1);  

  this->u0_ = floor(this->img_attr_.width/2);
  this->v0_ = floor(this->img_attr_.height/2);

  this->ff_ = 400;
  this->noise_ = 0;
}

//! Image Projection 
MatrixXd CImage_Based_Vs::image_projection(const float& fu, const float& fv, const MatrixXd& points)
{
  MatrixXd uv = MatrixXd::Zero(2,points.cols());
  uv.row(0) = (points.row(0).array()/points.row(2).array()) * fu + this->u0_;
  uv.row(1) = (points.row(1).array()/points.row(2).array()) * fv + this->v0_;
  return uv;
} 


//! Obtain Features Vector 
MatrixXd CImage_Based_Vs::features_vector(const MatrixXd& points)
{
  if (this->planar_){
    ArrayXXd xx(1,3),yy(1,3);
    MatrixXd s = MatrixXd::Zero(6,1);
    //features vector
    xx = (points.row(0).array()/points.row(2).array());
    yy = (points.row(1).array()/points.row(2).array());  
    //reshape (actually Eigen has no reshape function)
    s(0,0) = xx(0,0);
    s(1,0) = yy(0,0);
    s(2,0) = xx(0,1);
    s(3,0) = yy(0,1);
    s(4,0) = xx(0,2);
    s(5,0) = yy(0,2);
    return s;
  }
  else {
    ArrayXXd xx(1,4),yy(1,4);
    MatrixXd s = MatrixXd::Zero(8,1);
    //features vector
    xx = (points.row(0).array()/points.row(2).array());
    yy = (points.row(1).array()/points.row(2).array());  
    //reshape (actually Eigen has no reshape function)
    s(0,0) = xx(0,0);
    s(1,0) = yy(0,0);
    s(2,0) = xx(0,1);
    s(3,0) = yy(0,1);
    s(4,0) = xx(0,2);
    s(5,0) = yy(0,2);    
    s(6,0) = xx(0,3);  
    s(7,0) = yy(0,3);
    return s;
  }

}

//! Traditional Jacobian 
void CImage_Based_Vs::jacobian_traditional()
{
  if (this->planar_){
    ArrayXXd xx(1,3),yy(1,3),zz(1,3);
    //features vector
    xx=(this->base_in_cam_4d_.row(0).array()/this->base_in_cam_4d_.row(2).array());
    yy=(this->base_in_cam_4d_.row(1).array()/this->base_in_cam_4d_.row(2).array());  
    zz=(this->base_in_cam_4d_.row(2).array());
    //Computing Image Jacobian
    float xj,yj,zj;
    for (int i=0; i<3; ++i){
      xj=xx(0,i);
      yj=yy(0,i);
      zj=zz(0,i);
      this->J_.row((i*2)) << (-1.0/zj), 0.0, (xj/zj), (xj*yj), -(1.0 + (xj*xj)), yj;
      this->J_.row((i*2)+1) << 0.0, (-1.0/zj), (yj/zj), (1.0 + (yj*yj)), -(xj*yj), -xj;
    }
  }
  else {
    ArrayXXd xx(1,4),yy(1,4),zz(1,4);
    //features vector
    xx=(this->base_in_cam_4d_.row(0).array()/this->base_in_cam_4d_.row(2).array());
    yy=(this->base_in_cam_4d_.row(1).array()/this->base_in_cam_4d_.row(2).array());  
    zz=(this->base_in_cam_4d_.row(2).array());
    //Computing Image Jacobian
    float xj,yj,zj;
    for (int i=0; i<4; ++i){
      xj=xx(0,i);
      yj=yy(0,i);
      zj=zz(0,i);
      this->J_.row((i*2)) << (-1.0/zj), 0.0, (xj/zj), (xj*yj), -(1.0 + (xj*xj)), yj;
      this->J_.row((i*2)+1) << 0.0, (-1.0/zj), (yj/zj), (1.0 + (yj*yj)), -(xj*yj), -xj;
    }         
  }
}

//! Uncalibrated Jacobian 
void CImage_Based_Vs::jacobian_uncalibrated()
{

  // Planar		
  if (this->planar_) { 
    MatrixXd m;
    m = MatrixXd::Zero((2*this->n_),9);
    
    //Generate M and check the rank
    for (int i = 0; i < this->n_; ++i){
      for (int j = 0; j < 3; ++j){
	    //u rows
	    m((2*i),(j*3)) = this->alfas_in_obj_(j,i);
	    m((2*i),(j*3)+2) = this->alfas_in_obj_(j,i)*(this->u0_-this->uv_(0,i));
        //v rows
	    m((2*i)+1,(j*3)+1) = this->alfas_in_obj_(j,i);
	    m((2*i)+1,(j*3)+2) = this->alfas_in_obj_(j,i)*(this->v0_-this->uv_(1,i));
      }   
    }

    // Assemble the correlation matrix M = m' * m
    MatrixXd M(9,9),UU(9,9),VV(9,9);
    M = (m.transpose() * m);

    // Compute the Singular Value Decomposition
    SelfAdjointEigenSolver<Eigen::MatrixXd> eigensolver(M);
    VectorXd v(9,1);
    v = eigensolver.eigenvectors().col(0);

    //Distance equations
    int g = 0;
    Vector3d d,va,vb,vc,w;
    Vector2d b;
    MatrixXd L(3,2);
    L = MatrixXd::Zero(3,2);
    
    for (int i = 0; i < 2; ++i){
      for (int j = i+1; j < 3; ++j){
        va = v.block(i*3,0,3,1);
        vb = v.block(j*3,0,3,1);
        vc = va-vb;
        L(g,0) = (vc.topRows(2).transpose()*vc.topRows(2));
        L(g,1) = (vc(2)*vc(2));
        w = this->base_in_obj_3d_.col(i)-this->base_in_obj_3d_.col(j);
        d(g,0) = (w.transpose() * w);
        g = g+1;
      }
    }

    Matrix2d LL;
    MatrixXd L_inv(3,2);

    LL = (L.transpose()*L);
    L_inv = LL.inverse()*L.transpose();
    b(0) = L_inv.row(0) * d;
    b(1) = L_inv.row(1) * d;

    //Obtaining Beta and ff
    float Beta = +sqrt(fabs(b(0)));
    this->ff_ = +sqrt(fabs(b(1)))/Beta;

    VectorXd C2(9,1);

    C2 = Beta*v;

    //Check the sqrt correct sign
    Matrix3d check;
    check.col(0) = C2.block(0,0,3,1);
    check.col(1) = C2.block(3,0,3,1);
    check.col(2) = C2.block(6,0,3,1);
    
    if (check.determinant()>0){
      C2 = -C2;  
    }
    C2(2,0) = C2(2,0)*this->ff_;
    C2(5,0) = C2(5,0)*this->ff_;
    C2(8,0) = C2(8,0)*this->ff_;

    //Computing Image Jacobian
    float xj,yj,zj;
    for (int i = 0; i < 3; ++i){
      xj = C2((i*3),0);
      yj = C2((i*3)+1,0);
      zj = C2((i*3)+2,0);
      this->J_.row((i*2)) << (-1.0/zj), 0.0, (xj/zj), (xj*yj), -(1.0 + (xj*xj)), yj;
      this->J_.row((i*2)+1) << 0.0, (-1.0/zj), (yj/zj), (1.0 + (yj*yj)), -(xj*yj), -xj;
    }
  }

  // NON Planar  
  else {

    MatrixXd m;
    m = Eigen::MatrixXd::Zero((2*this->n_),12);

    //Generate M and check the rank
    for (int i = 0; i < this->n_; ++i){
      for (int j = 0; j < 4; ++j){
	    //u rows
	    m((2*i),(j*3))=this->alfas_in_obj_(j,i);
	    m((2*i),(j*3)+2)=this->alfas_in_obj_(j,i)*(this->u0_-this->uv_(0,i));
    	//v rows
	    m((2*i)+1,(j*3)+1)=this->alfas_in_obj_(j,i);
	    m((2*i)+1,(j*3)+2)=this->alfas_in_obj_(j,i)*(this->v0_-this->uv_(1,i));
      }   
    }

    // Assemble the correlation matrix M = m' * m
    MatrixXd M(12,12),UU(12,12),VV(12,12);
    M = (m.transpose() * m);

    // Compute the Singular Value Decomposition
    SelfAdjointEigenSolver<MatrixXd> eigensolver(M);
    VectorXd v(12,1);
    v = eigensolver.eigenvectors().col(0);

     //Distance equations
    int g = 0;
    Vector3d va,vb,vc,w;
    VectorXd d(6,1);
    Vector2d b;
    MatrixXd L;
    L = MatrixXd::Zero(6,2);
    
    for (int i = 0; i < 3; ++i){
      for (int j = i+1; j < 4; ++j){
	    va = v.block(i*3,0,3,1);
	    vb = v.block(j*3,0,3,1);
	    vc = va-vb;
	    L(g,0) = (vc.topRows(2).transpose()*vc.topRows(2));
	    L(g,1) = (vc(2)*vc(2));
	    w = this->base_in_obj_3d_.col(i)-this->base_in_obj_3d_.col(j);
	    d(g,0) = (w.transpose() * w);
	    g = g+1;
      }
    }

    Matrix2d LL;
    MatrixXd L_inv(6,2);

    LL = (L.transpose() * L);
    L_inv = LL.inverse() * L.transpose();
    b(0) = L_inv.row(0) * d;
    b(1) = L_inv.row(1) * d;
    
    //Obtaining Beta and ff
    float Beta = +sqrt(fabs(b(0)));
    this->ff_ = +sqrt(fabs(b(1)))/Beta;
    
    VectorXd C2(12,1);

    C2 = Beta*v;

    //Check the sqrt correct sign
    Vector3d cc1,cc2,cc3;
    cc1 = (C2.block(3,0,3,1)-C2.block(0,0,3,1));
    cc2 = (C2.block(6,0,3,1)-C2.block(0,0,3,1));
    cc3 = (C2.block(9,0,3,1)-C2.block(0,0,3,1));
     
    Vector3d ccc1,ccc2,ccc3;
    ccc1 = cc1.cross(cc2);
    ccc2 = cc1.cross(cc3);
    ccc3 = cc2.cross(cc3);
     
    float oo1,oo2,oo3; 
    oo1 = (ccc1.transpose()*cc3);
    oo2 = (ccc2.transpose()*cc2);
    oo3 = (ccc3.transpose()*cc1);
     
    if ((oo1 < 0) || (oo2>0) || (oo3<0)){
      C2 = -C2;  
    }
    C2(2,0) = C2(2,0)*this->ff_;
    C2(5,0) = C2(5,0)*this->ff_;
    C2(8,0) = C2(8,0)*this->ff_;
    C2(11,0) = C2(11,0)*this->ff_;
    
    //Computing Image Jacobian
    float xj,yj,zj;
    for (int i = 0; i < 4; ++i){
      xj = C2((i*3),0);
      yj = C2((i*3)+1,0);
      zj = C2((i*3)+2,0);
      this->J_.row((i*2)) << (-1.0/zj), 0.0, (xj/zj), (xj*yj), -(1.0 + (xj*xj)), yj;
      this->J_.row((i*2)+1) << 0.0, (-1.0/zj), (yj/zj), (1.0 + (yj*yj)), -(xj*yj), -xj;
    }   
  }
}

//! Control 
void CImage_Based_Vs::control()
{

  this->vel_ = MatrixXd::Zero(6,1);	

  if (this->planar_) {
    // features error
    this->diff_ = this->s_.array()-this->s_x_.array();

    //Proportional control
    MatrixXd J_pseudo(6,6);
    J_pseudo = MatrixXd::Zero(6,6);
    J_pseudo = this->J_.inverse();


    this->vel_ = J_pseudo * this->diff_;
  }
  else {
    // features error
    this->diff_ = this->s_.array()-this->s_x_.array();
    
    MatrixXd JJ(6,6);

    JJ = this->J_.transpose()*this->J_;
    MatrixXd J_pseudo(8,6);
    J_pseudo = MatrixXd::Zero(8,6);
    J_pseudo = JJ.inverse() * this->J_.transpose();  

    this->vel_ = J_pseudo * this->diff_;
  }
}

//! Special quadrotor underactuated output (pitch and roll velocities to 0) 
void CImage_Based_Vs::quadrotor_ctrl()
{        
  
  this->vel_ = MatrixXd::Zero(6,1);

  // Extract the respective columns of the non controllable DOF
  MatrixXd J1(8,4),J2(8,2),V1(4,1);
  J1.col(0) = this->J_.col(0);
  J1.col(1) = this->J_.col(1);
  J1.col(2) = this->J_.col(2);
  J1.col(3) = this->J_.col(5);
  J2.col(0) = this->J_.col(3);
  J2.col(1) = this->J_.col(4);

  MatrixXd JJ1(4,4);
  MatrixXd J1_pseudo(4,8);
  
  //pseudo inverse of J1
  JJ1 = J1.transpose()*J1;
  J1_pseudo = JJ1.inverse()*J1.transpose();  
 
  // Control law
  V1 = J1_pseudo * (this->diff_ + (J2 * this->V2_));

  // Set to 0 the non controllable DOF velocities	      
  MatrixXd Velq(6,1);
  Velq = MatrixXd::Zero(6,1);
  Velq.block(0,0,3,1) = V1.block(0,0,3,1);
  Velq(5,0) = V1(3,0);
      
  this->vel_ = Velq.block(0,0,6,1);
}

//! Set angles between -pi and pi
double CImage_Based_Vs::pi2pi(double& angle)
{
  double pi = 3.14159265359;
  if (angle >= pi) angle = angle-2*pi;
  if (angle <= -pi) angle = angle+2*pi;
  return angle;
}

//! Rotate velocities to camera frame
MatrixXd CImage_Based_Vs::cam_frame_change(const MatrixXd& vel_base)
{
  MatrixXd cam_vel = MatrixXd::Zero(6,1);

  cam_vel(0,0)=-vel_base(0,0);
  cam_vel(1,0)=-vel_base(1,0);
  cam_vel(2,0)=vel_base(2,0);
  cam_vel(3,0)=vel_base(3,0);
  cam_vel(4,0)=vel_base(4,0);
  cam_vel(5,0)=-vel_base(5,0);

  return cam_vel; 
}

Matrix4d CImage_Based_Vs::get_HT(const double& x,const double& y,const double& z,const double& roll,const double& pitch,const double& yaw)
{
  // euler convention zyx
  Eigen::Matrix3d Rot; 
  Rot = Eigen::AngleAxisd(yaw, Eigen::Vector3d::UnitZ()) \
    * Eigen::AngleAxisd(pitch, Eigen::Vector3d::UnitY()) \
    * Eigen::AngleAxisd(roll, Eigen::Vector3d::UnitX());
   
  Eigen::Matrix4d HT(4,4);
  HT.block(0,0,3,3)=Rot;  
  HT.block(0,3,3,1) << x, y, z;
  HT.row(3) << 0.0, 0.0, 0.0, 1.0;

  return HT;
}

void CImage_Based_Vs::get_features(MatrixXd& features, MatrixXd& base)
{
 features = this->features_in_obj_;
 base =  this->base_in_obj_4d_;
}