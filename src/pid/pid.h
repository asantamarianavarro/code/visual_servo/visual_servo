#ifndef _pid_h_
#define _pid_h_

class PID {

  private:

    double prev_error_; // Previous error
    double int_error_; // Integral error

  public:

    double windup_guard_; // Integral limit
    double kp_; // Proportional gain
    double ki_; // Integrative gain
    double kd_; // Derivative gain
    double control_;  // Control signal output

   /**
    * \brief Constructor
    * 
    */
    PID(void);

   /**
    * \brief Destructor
    * 
    */
    ~PID(void);

    /**
    * \brief PID reset
    *
    * Reset Integral and derivative errors
    *
    */
    void reset(void);

    /**
    * \brief update PID output
    *
    * Update PID obtaining control values
    *
    */
    void update(double curr_error, double dt);  
};

#endif