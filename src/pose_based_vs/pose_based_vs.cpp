#include "pose_based_vs.h"

using namespace Eigen;
using namespace std;

CPose_Based_Vs::CPose_Based_Vs()
{
  // Setup PIDs
  this->pid_.resize(6);
  for (int ii = 0; ii < this->pid_.size(); ++ii)
  {
    this->pid_[ii].windup_guard_ = 0.0;
    this->pid_[ii].kp_ = 0.0;
    this->pid_[ii].ki_ = 0.0;
    this->pid_[ii].kd_ = 0.0;
    this->pid_[ii].control_ = 0.0; 
    this->pid_[ii].reset();
  }
}
 
CPose_Based_Vs::~CPose_Based_Vs()
{
}

//! Main public function call of Image-Based Visual Servo.
/*!
  \param desired_pose (Eigen::MatrixXd [4,4]): Desired position (Homogenous transform).
  \param current_pose (Eigen::MatrixXd [4,4]): Current target position r.t. camera frame (Homogenous transform).
  \param dt (double): time interval for control.
  \param kp (Eigen::MatrixXd [6,1]): Proportional gain.
  \param kd (Eigen::MatrixXd [6,1]): Derivative gain.
  \param ki (Eigen::MatrixXd [6,1]): Integral gain.
  \param i_lim (double): Integration limits.
  \param cam_vel (Eigen::MatrixXd [6,1]): Output camera velocities.
  \return The camera velocities
*/
void CPose_Based_Vs::pose_based_vs(const MatrixXd& desired_pose,
                                   const MatrixXd& current_pose,
                                   const double& dt,
                                   const MatrixXd& kp,
                                   const MatrixXd& kd,
                                   const MatrixXd& ki,
                                   const double& i_lim,
                                   MatrixXd& error,
                                   MatrixXd& cam_vel)
{
  this->current_pose_=current_pose;
  this->desired_pose_=desired_pose;

  if (dt==0.0)
  {
    for (int ii = 0; ii < this->pid_.size(); ++ii)
    {
      this->pid_[ii].reset();
      this->pid_[ii].kp_ = kp(ii,0);
      this->pid_[ii].ki_ = ki(ii,0);
      this->pid_[ii].kd_ = kd(ii,0);
      this->pid_[ii].windup_guard_ = i_lim;
    }
  }
    
  cam_vel = MatrixXd::Zero(6,1);

  //pose error computation
  error = MatrixXd::Zero(6,1);

  for (int ii = 0; ii < this->current_pose_.rows(); ++ii)
  {
   	//compute the error	
    error(ii,0) = this->desired_pose_(ii,0) - this->current_pose_(ii,0);
   	// error(ii,0) = tanh(this->desired_pose_(ii,0) - this->current_pose_(ii,0));

    // Only for angular values
    if(ii > 2) 
    {
      double err = error(ii,0);
      error(ii,0) = pi2pi(err);
    }

   	// //compute velocities with the PID
   	// cam_vel(ii,0)=updatePid(error(ii,0), this->dt_, ii);  

    //compute control commands
    // this->pid_[ii].update(-error(ii,0), dt);
    this->pid_[ii].update(error(ii,0), dt);
    cam_vel(ii,0) = this->pid_[ii].control_;
  }
}

//! Set angles between -pi and pi
double CPose_Based_Vs::pi2pi(double& angle)
{
  double pi = 3.14159265359;
  if (angle >= pi) angle = angle-2*pi;
  if (angle <= -pi) angle = angle+2*pi;
  return angle;
}