#ifndef _POSE_BASED_VS_H
#define _POSE_BASED_VS_H

#include <stdio.h>
#include <iostream>
#include <string>

#include <eigen3/Eigen/Dense>

#include <pid.h>
#include <vector>

using namespace Eigen;

//!  Pose-Based Visual Servoing Class 
/*!
  This Class Implements a Pose-Based Visual servoing 
*/
class CPose_Based_Vs
{

  private:

    MatrixXd current_pose_; /*!< [6,1] Target position and orientation in camera frame */
    MatrixXd desired_pose_; /*!< [6,1] Desired final position */

    // PID parameters
    std::vector<PID> pid_; /*! PID controllers */

    /**
    * \brief PID
    *
    * PID update.
    *
    */
	// double updatePid(double& error,const double& dt,const double& i);

    /**
    * \brief Pi to Pi
    *
    * Set an angle between -pi and pi (rad).
    *
    */
    double pi2pi(double& angle);

  public:

    /**
    * \brief Constructor
    *
    * Pose-Based Visual Servoing Class constructor.
    *
    */
    CPose_Based_Vs();

    /**
    * \brief Destructor
    *
    * Pose-Based Visual Servoing Class destructor.
    *
    */    
    ~CPose_Based_Vs();

    /**
    * \brief pose-Based Visual Servo Function
    *
    * Main public function call of Pose-Based Visual Servo.
    *
    */  
    void pose_based_vs(const MatrixXd& desired_pose,
                       const MatrixXd& current_pose,
                       const double& dt, 
                       const MatrixXd& kp,
                       const MatrixXd& kd,
                       const MatrixXd& ki,
                       const double& i_lim,
                       MatrixXd& error,
                       MatrixXd& cam_vel);
};

#endif
