#edit the following line to add the librarie's header files
FIND_PATH(visual_servo_INCLUDE_DIR image_based_vs.h pose_based_vs.h /usr/include/iridrivers /usr/local/include/iridrivers)

FIND_LIBRARY(visual_servo_LIBRARY
    NAMES visual_servo
    PATHS /usr/lib /usr/local/lib /usr/local/lib/iridrivers) 

IF (visual_servo_INCLUDE_DIR AND visual_servo_LIBRARY)
   SET(visual_servo_FOUND TRUE)
ENDIF (visual_servo_INCLUDE_DIR AND visual_servo_LIBRARY)

IF (visual_servo_FOUND)
   IF (NOT visual_servo_FIND_QUIETLY)
      MESSAGE(STATUS "Found visual_servo: ${visual_servo_LIBRARY}")
   ENDIF (NOT visual_servo_FIND_QUIETLY)
ELSE (visual_servo_FOUND)
   IF (visual_servo_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find visual_servo")
   ENDIF (visual_servo_FIND_REQUIRED)
ENDIF (visual_servo_FOUND)

